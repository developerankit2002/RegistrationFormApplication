package com.example.registrationformapplication.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.registrationformapplication.R
import com.example.registrationformapplication.databinding.ActivityLoginBinding
import com.example.registrationformapplication.factory.SignUpSharedPrefRepositoryViewModalFactory
import com.example.registrationformapplication.repository.SignUpSharedPrefRepository
import com.example.registrationformapplication.utility.Keys
import com.example.registrationformapplication.viewModal.SignUpSharedPrefRepositoryViewModel

class LoginActivity : AppCompatActivity(), View.OnClickListener {

    //----------------------------------------------------------------------------------------------
    private lateinit var binding : ActivityLoginBinding
    private lateinit var viewModel : SignUpSharedPrefRepositoryViewModel
    private lateinit var factory : SignUpSharedPrefRepositoryViewModalFactory
    //----------------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------------
    override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
        factory = SignUpSharedPrefRepositoryViewModalFactory(SignUpSharedPrefRepository, this)
        viewModel = ViewModelProvider(this, factory)[SignUpSharedPrefRepositoryViewModel::class.java]
        binding.btnLogin.setOnClickListener(this)
        binding.tvSkiplogin.setOnClickListener(this)
        binding.tvDonthaveaccount.setOnClickListener(this)
        binding.mylogindata = viewModel
        binding.lifecycleOwner = this
    }
    //----------------------------------------------------------------------------------------------


    //----------------------------- Start of Button Click Event ------------------------------------
    override fun onClick(view : View?) {
        when (view?.id) {
            R.id.btn_login -> {
                if (viewModel.loginNumberIsValidOrNot()) {
                    Toast.makeText(this, " Number is Not Valid ", Toast.LENGTH_SHORT).show()
                } else {
                    val getResult = viewModel.getData()
                    if (getResult == "Admin" || getResult == "HR" || getResult == "Consultant" || getResult == "Faculty") {
                        val intent = Intent(this, FormActivity::class.java)
                        intent.putExtra(Keys.USER_TYPE, getResult)
                        startActivity(intent)
                    } else {
                        Toast.makeText(this, getResult, Toast.LENGTH_SHORT).show()
                    }
                }

            }


            R.id.tv_skiplogin -> {
                val intent = Intent(this, FormActivity::class.java)
                intent.putExtra(Keys.USER_TYPE, "Guest")
                startActivity(intent)
            }


            R.id.tv_donthaveaccount -> {
                val intent = Intent(this, SignUpActivity::class.java)
                startActivity(intent)
            }

        }

    }
    //---------------------------------- End of Buttons Click Event --------------------------------

}
//------------------------------------ End of Main Thread ------------------------------------------