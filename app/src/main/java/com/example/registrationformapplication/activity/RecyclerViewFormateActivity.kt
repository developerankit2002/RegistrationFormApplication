package com.example.registrationformapplication.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.registrationformapplication.R
import com.example.registrationformapplication.adapters.ListViewAdapter
import com.example.registrationformapplication.databinding.ActivityRecyclerViewFormateBinding
import com.example.registrationformapplication.factory.FormActivityViewModelFactory
import com.example.registrationformapplication.repository.SqliteDBRepository
import com.example.registrationformapplication.viewModal.FormActivityViewModel

class RecyclerViewFormateActivity : AppCompatActivity(){
    private lateinit var binding : ActivityRecyclerViewFormateBinding
    private lateinit var viewModel : FormActivityViewModel
    private lateinit var factory: FormActivityViewModelFactory
    @SuppressLint("RestrictedApi")
    public override fun onCreate(savedInstanceState : Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_recycler_view_formate)
        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        factory=FormActivityViewModelFactory(SqliteDBRepository(this))
        viewModel = ViewModelProvider(this,factory)[FormActivityViewModel::class.java]
        setListview()
    }
    private fun setListview(){
        val listOfStudents = viewModel.getAllData()
        val myAdapter = ListViewAdapter(listOfStudents)
        binding.listView.adapter = myAdapter
    }

}