package com.example.registrationformapplication.obserable

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR

class FormFieldObserable : BaseObservable() {
    @get:Bindable
    var fnameform : String = ""
        set(value) {
            field = value
            notifyPropertyChanged(/* fieldId = */ BR.fnameform)
        }

    @get:Bindable
    var lnameform : String = ""
        set(value) {
            field = value
            notifyPropertyChanged(/* fieldId = */ BR.lnameform)
        }

    @get:Bindable
    var mobilenumberform : String = ""
        set(value) {
            field = value
            notifyPropertyChanged(/* fieldId = */ BR.mobilenumberform)
        }

    @get:Bindable
    var altnumberform : String = ""
        set(value) {
            field = value
            notifyPropertyChanged(/* fieldId = */ BR.altnumberform)
        }



    @get:Bindable
    var dobform:String=""
    set(value) {
        field=value
        notifyPropertyChanged(BR.dobform)
    }



    @get:Bindable
    var emailform:String=""
    set(value) {
        field=value
        notifyPropertyChanged(/* fieldId = */ BR.emailform)
    }

    @get:Bindable
    var addressform:String=""
    set(value) {
        field=value
        notifyPropertyChanged(/* fieldId = */ BR.addressform)
    }

}