package com.example.registrationformapplication.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import com.example.registrationformapplication.R
import com.example.registrationformapplication.databinding.LayoutItemsBinding
import com.example.registrationformapplication.model.Student

class ListViewAdapter(private val listOfStudents: ArrayList<Student>): BaseAdapter() {
    override fun getCount(): Int {
        return listOfStudents.size
    }

    override fun getItem(position: Int): Any {
        return listOfStudents[position]
    }

    override fun getItemId(pos: Int): Long {
        return listOfStudents[pos].hashCode().toLong()
    }

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view=convertView
        view=LayoutInflater.from(parent!!.context).inflate(R.layout.layout_items,parent,false)
        val binding=DataBindingUtil.bind<LayoutItemsBinding>(view)
        binding!!.tvFname.text=listOfStudents[position].fName
        binding.tvLname.text=listOfStudents[position].lName
        view.setOnClickListener{
            Toast.makeText(parent.context, listOfStudents[position].srNo, Toast.LENGTH_SHORT).show()
        }
        return view
    }
}