package com.example.registrationformapplication.viewModal

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.ViewModel
import com.example.registrationformapplication.obserable.SignUpFieldObserable
import com.example.registrationformapplication.repository.SignUpSharedPrefRepository
import com.example.registrationformapplication.utility.Keys

class SignUpSharedPrefRepositoryViewModel(
    private val signupsharedPrefRepo : SignUpSharedPrefRepository,
    private val context : Context,
) : ViewModel() {
    private var firstName : String = " "
    private var lastname : String = " "
    private var mobileNumberSignUp : String = " "
    private var mobileNumberLogin : String = ""

    var signupfieldObserabl : SignUpFieldObserable = SignUpFieldObserable()

    //------------------Data Save in Repository-----------------------------------------------------


    fun saveData(userType : String) : Boolean {
        //-------------------Data Get From Base Obserable Class Fields------------------------------
        firstName = signupfieldObserabl.fnamesignup
        lastname = signupfieldObserabl.lnamesignup
        mobileNumberSignUp = signupfieldObserabl.mobileNumberSignUp

        if (firstName.isEmpty() && lastname.isEmpty() && mobileNumberSignUp.isEmpty() && userType == "-select-") {
            Toast.makeText(context, " All Field is Empty ", Toast.LENGTH_SHORT).show()
            return false
        } else {

            if (userType == "-select-") {
                Toast.makeText(context, " Please Select User Type ", Toast.LENGTH_SHORT).show()
                return false
            } else if (firstName == "") {
                Toast.makeText(context, " First Name Can't be Empty ", Toast.LENGTH_SHORT).show()
                return false
            } else if (lastname == "") {
                Toast.makeText(context, " Last Name Can't be Empty ", Toast.LENGTH_SHORT).show()
                return false
            } else if (mobileNumberSignUp.length != 10) {
                Toast.makeText(context, " Please Enter Valid Mobile Number ", Toast.LENGTH_SHORT)
                    .show()
                return false
            } else {

                signupsharedPrefRepo.getPreference(context).edit()
                    .putString(Keys.FNAME, firstName).apply()
                signupsharedPrefRepo.getPreference(context).edit()
                    .putString(Keys.LNAME, lastname).apply()
                signupsharedPrefRepo.getPreference(context).edit()
                    .putString(Keys.MOBNO, mobileNumberSignUp).apply()
                signupsharedPrefRepo.getPreference(context).edit()
                    .putString(Keys.USER_TYPE, userType).apply()
                return true
            }
        }
    }

    fun loginNumberIsValidOrNot() : Boolean {
        mobileNumberLogin = signupfieldObserabl.mobileNumberLogin
        return mobileNumberLogin.length != 10
    }

    //-----------------Data Get From Repository-----------------------------------------------------
    fun getData() : String {
        val mobileNumberSignUp =
            signupsharedPrefRepo.getPreference(context).getString(Keys.MOBNO, "").toString()
        val getUserTypeFromSharedPref =
            signupsharedPrefRepo.getPreference(context).getString(Keys.USER_TYPE, "").toString()
        val messageForUser = " This Number is Not Exist "

        return if (mobileNumberSignUp == mobileNumberLogin) {
            getUserTypeFromSharedPref
        } else {
            messageForUser
        }
    }

}
